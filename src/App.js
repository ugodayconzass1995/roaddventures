import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ParallaxProvider } from 'react-scroll-parallax';
import HomePage from './pages/Home/index'

const App = () => {
  return (
    <ParallaxProvider>
      <Router>
        <Routes>
          <Route path="/" element={<HomePage />} />
        </Routes>
      </Router>
    </ParallaxProvider>
  );
}

export default App;
