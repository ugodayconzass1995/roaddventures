import React from "react";
import { Parallax } from "react-scroll-parallax";
import useWindowDimensions from "../../useWindowDimensions.js";
import "../Agenda/agenda.css";

const Agenda = () => {
  const { width } = useWindowDimensions();

  return (
    <section id="agenda">
      <div className="container">
        <Parallax speed={width > 820 ? 14 : 6}>
          <div className="agenda__head-text">
            Cоздайте воспоминания, которые будут с вами всегда
          </div>
        </Parallax>
        <Parallax speed={width > 820 ? 10 : 4}>
          <div className="agenda__main-text">
            Мы стремимся сделать путешествия доступными для каждого, независимо
            от бюджета, предпочтений или опыта. Мы хотим подарить каждому
            возможность увидеть мир глазами путешественника, почувствовать
            восторг открытий и ощутить вдохновение, которое приносит
            исследование новых мест
          </div>
        </Parallax>
        <Parallax speed={width > 820 ? 13 : 6}>
          <div className="agenda__footer-text">
            Путешествия - это не только источник удовольствия, но и средство
            проникновения в мир разнообразия, обогащения души <br /> и
            расширения горизонтов.
          </div>
        </Parallax>
      </div>
    </section>
  );
};

export default Agenda;
