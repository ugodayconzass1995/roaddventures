import React from "react";
import plane from "../../assets/imgs/plane-with-a-path.svg";
import planeMobile from "../../assets/imgs/plane-with-a-path-mobile.png";
import { Fade, AttentionSeeker } from "react-awesome-reveal";
import "../Book/book.css";

const Book = () => {
  return (
    <section id="book">
      <img className="book__plane-with-a-path" src={plane} alt="" />
      <img
        className="book__plane-with-a-path-mobile"
        src={planeMobile}
        alt=""
      />
      <div className="container">
        <Fade delay="10" duration="1000">
          <h2 className="book__title">
            <span>Время</span> действовать!
          </h2>
        </Fade>
        <Fade delay="20" duration="1500">
          <p className="book__text">
            Заполните нашу анкету и дайте нам знать о своих мечтах и
            предпочтениях. Мы воплотим твои идеи в реальность и подготовим
            специальный тур, который будет полностью соответствовать твоим
            ожиданиям. Откройте новые горизонты вместе с нами!
          </p>
        </Fade>
        <Fade delay="30" duration="2000">
          <div className="book__signature">
            <span>Открой новые горизонты вместе с нами!</span>
          </div>
        </Fade>
        <AttentionSeeker effect="heartBeat" delay="50" duration="2000">
          <a
            href="https://view.forms.app/roaddventures/form"
            className="book__btn"
          >
            подобрать тур
          </a>
        </AttentionSeeker>
      </div>
    </section>
  );
};

export default Book;
