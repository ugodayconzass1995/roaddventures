import React from "react";
import "../Footer/footer.css";
import instagram from "../../assets/imgs/instagram.svg";
import whatsapp from "../../assets/imgs/whatsapp.svg";
import logo from "../../assets/imgs/logo.svg";
import telegram from "../../assets/imgs/telegram.svg";

const FooterMenu = () => {
  const menu = [
    {
      link: "#offer",
      title: "Маршруты",
    },
    {
      link: "#people-choose",
      title: "Популярное",
    },
    {
      link: "#steps",
      title: "Этапы работы",
    },
    {
      link: "#book",
      title: "Оставить заявку",
    },
    {
      link: "#reviews",
      title: "Отзывы",
    },
    {
      link: "#faq",
      title: "FAQ",
    },
  ];

  return (
    <nav className="footer__menu">
      <ul className="first-line-menu">
        {menu.map((item) => (
          <li key={item.title}>
            <a href={item.link}>{item.title}</a>
          </li>
        ))}
      </ul>
      <a
        className="footer__menu_btn"
        href="https://view.forms.app/roaddventures/form"
      >
        подобрать тур
      </a>
    </nav>
  );
};

const FooterContact = () => {
  return (
    <div className="footer-contact">
      <div>
        <div className="footer-call">Позвонить</div>
        <a href="tel:+7 (777) 225 6260" className="footer-call__number">
          +7 (777) 225 6260
        </a>
      </div>
      <div className="footer-contact__line"></div>
      <div>
        <div className="footer-email">написать</div>
        <a
          href="mailto:roddventures@gmail.com"
          className="footer-email__address"
        >
          roddventures@gmail.com
        </a>
      </div>
    </div>
  );
};

const FooterSocialIcons = () => {
  return (
    <div className="footer-social__icons">
      <a
        target="_blank"
        href="https://instagram.com/roaddventures?igshid=NGVhN2U2NjQ0Yg%3D%3D&utm_source=qr"
        className="footer-social__ig"
        rel="noreferrer"
      >
        <img src={instagram} alt="Instagram" />
      </a>
      <a
        href="https://wa.me/77772256260"
        className="footer-social__wapp"
        target="_blank"
        rel="noreferrer"
      >
        <img src={whatsapp} alt="What'sApp icon" />
      </a>
      <a
        target="_blank"
        href="https://t.me/roaddventures"
        className="footer-social__tg"
        rel="noreferrer"
      >
        <img src={telegram} alt="Telegram icon" />
      </a>
    </div>
  );
};

const Footer = () => {
  return (
    <footer className="footer-bg">
      <div className="footer__top">
        <a href="#header">
          <img src={logo} alt="" />
        </a>
      </div>
      <div className="line-first"></div>
      <FooterMenu />
      <div className="line-second"></div>
      <FooterContact />
      <FooterSocialIcons />
      <div className="footer-copyright">
        &copy; 2023 Roaddventures. All rights reserved
      </div>
    </footer>
  );
};

export default Footer;
