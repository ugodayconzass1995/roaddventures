import React from "react";
import { Fade, AttentionSeeker } from "react-awesome-reveal";
import ArrowMenuMobile from "../../assets/imgs/arrow-menu-mobile.svg";
import instagram from "../../assets/imgs/instagram.svg";
import whatsapp from "../../assets/imgs/whatsapp.svg";
import logo from "../../assets/imgs/logo.svg";
import mainBg from "../../assets/imgs/main-bg.jpg";
import telegram from "../../assets/imgs/telegram.svg";
import "./header.css";

const Header = () => {
  const menu = [
    {
      link: "#offer",
      title: "Маршруты",
    },
    {
      link: "#people-choose",
      title: "Популярное",
    },
    {
      link: "#steps",
      title: "Этапы работы",
    },
    {
      link: "#book",
      title: "Оставить заявку",
    },
    {
      link: "#reviews",
      title: "Отзывы",
    },
    {
      link: "#faq",
      title: "FAQ",
    },
  ];

  return (
    <header id="header">
      <img src={mainBg} className="header__main-bg" alt="" />
      <div className="container">
        <div className="header__top">
          <a href="./">
            <img src={logo} alt="" />
          </a>
          <nav className="header__menu">
            <ul>
              {menu.map((item) => (
                <li key={item.title}>
                  <a href={item.link}>{item.title}</a>
                </li>
              ))}
            </ul>
          </nav>
          <div className="header__mobile-menu">
            <div
              onClick={() => document.documentElement.classList.add("act-menu")}
              className="header__menu_burger"
            >
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
        </div>
        <nav className="header__mobile_nav">
          <ul>
            {menu.map((item) => (
              <li key={item.title}>
                <a
                  onClick={() =>
                    document.documentElement.classList.remove("act-menu")
                  }
                  href={item.link}
                >
                  {item.title}
                </a>
              </li>
            ))}
          </ul>
          <div className="mobile-menu__contacts">
            <div className="mobile-menu__contacts_title">Контакты</div>
            <a href="tel:+7 (777) 225 6260">+7 (777) 225 6260</a>
            <a href="mailto:roddventures@gmail.com">roddventures@gmail.com</a>
            <div className="mobile-menu__socials">
              <a
                target="_blank"
                href="https://instagram.com/roaddventures?igshid=NGVhN2U2NjQ0Yg%3D%3D&utm_source=qr"
                className="footer-social__ig"
                rel="noreferrer"
              >
                <img src={instagram} alt="Instagram" />
              </a>
              <a
                href="https://wa.me/77772256260"
                className="footer-social__wapp"
                target="_blank"
                rel="noreferrer"
              >
                <img src={whatsapp} alt="What'sApp icon" />
              </a>
              <a
                target="_blank"
                href="https://t.me/roaddventures"
                className="footer-social__tg"
                rel="noreferrer"
              >
                <img src={telegram} alt="Telegram icon" />
              </a>
            </div>
          </div>
          <div
            onClick={() =>
              document.documentElement.classList.remove("act-menu")
            }
            className="header__mobile_close"
          >
            <img src={ArrowMenuMobile} alt="" /> Назад
          </div>
        </nav>
        <div className="header__block">
          <div className="header__title">
            <Fade delay="50" duration="1500">
              <h1>EXPLORE</h1>
            </Fade>
            <Fade delay="100" duration="500">
              <div className="signature">вместе с Ro&ddventures</div>
            </Fade>
          </div>
          <div className="header__desc">
            Открой мир уникальных путешествий: <br />
            Ваш тур, Ваша история!
          </div>
          <AttentionSeeker effect="heartBeat" delay="50" duration="2000">
            <div className="header__btn">
              <a href="https://view.forms.app/roaddventures/form">
                подобрать тур
              </a>
            </div>
          </AttentionSeeker>
        </div>
      </div>
    </header>
  );
};

export default Header;
