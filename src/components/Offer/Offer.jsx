import React, { useState, useEffect } from "react";
import arrowComments from "../../assets/imgs/arrow-comment.svg";
import { Parallax, ParallaxBanner } from "react-scroll-parallax";
import useWindowDimensions from "../../useWindowDimensions.js";
import "../Offer/offer.css";

const Offer = () => {
  const { width } = useWindowDimensions();
  const offers = [
    {
      id: "Культура",
      img: "culture.webp",
      title: "Культура",
      desc: "Станьте свидетелем очарования старого мира, путешествуя по имперскому городу Стамбулу, в котором смешались азиатская и европейская культуры, услышьте звон церковных колоколов в созвучии с намазом и звук шофара в объединенной обители иудаизма, ислама и христианства Иерусалиме, всмотритесь в каждый уголок острова Бали, который пропитан культурными изысками и традициями.",
    },
    {
      id: "Приключения",
      img: "adventures.webp",
      title: "Приключения",
      desc: "От потрясающих пейзажей Вади Рам до древних руин Петры, Иордания предложит вам уникальные и незабываемые приключения. Насладитесь Исландией - удивительной страной на северо-западе Европы, известной своими вулканами, гейзерами, ледяными полями и прекрасными ландшафтами, а также воспользуйтесь предложениями Бразилии с незабываемыми приключениями в Амазонском бассейне. ",
    },
    {
      id: "Уединение",
      img: "privacy.webp",
      title: "Уединение",
      desc: "Почувствуйте приватность и возможность уединения на по-настоящему уникальных мальдивских курортах, насладитесь белоснежными пляжами, прекрасными закатами и кристально чистыми водами Индийского океана. Благодаря пустынным пляжам, кристально чистой воде и атмосфере хиппи, окунитесь в красоты Фуэртевентуры на Канарских островах. ",
    },
    {
      id: "Пейзажи",
      img: "landscape.webp",
      title: "Пейзажи",
      desc: "Красота природы поражает своим разнообразием и великолепием. От национальных парков до гор, от водопадов до пустынь - эти пейзажи оставляют незабываемые впечатления. Путешествуйте по местам, где можно увидеть эти красоты, ведь это является одним из самых незабываемых опытов в жизни. ",
    },
    {
      id: "Урбан",
      img: "urban.webp",
      title: "Урбан",
      desc: "Города США и Европы - это уникальное сочетание истории, культуры и современности. Насладитесь шумом Нью-Йорка и Мадрида, прекрасными пляжами Майами и Лос-Анджелеса, удивительными музеями Вены, красотой Барселоны и Венеции, а также многими другими городами, которые оставят незабываемые впечатления.",
    },
  ];

  const [actOffer, setActOffer] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      if (actOffer === offers.length - 1) {
        setActOffer(0);
      } else {
        setActOffer((actOffer) => actOffer + 1);
      }
    }, 8000);
    return () => clearInterval(interval);
  });

  return (
    <section id="offer">
      <div className="container">
        <div className="offer__head">
          <Parallax
            className="offer__signature signature-1"
            disabled={width > 820 ? false : true}
            speed={4}
          >
            Есть сомнения?
          </Parallax>
          <div className="offer__signature signature-2">
            Cледуй нашим готовым маршрутам!
          </div>
          <Parallax speed={5}>
            <h2 className="offer__title">
              <span className="offer__title_first">Мы</span>
              <span className="offer__title_second">предлагаем</span>
            </h2>
          </Parallax>
        </div>
        <div className="offer__body">
          <div className="offer__slider">
            <div className="offer__content">
              <Parallax disabled={width > 820 ? false : true} speed={8}>
                <h5>{offers[actOffer].title}</h5>
              </Parallax>
              <Parallax disabled={width > 820 ? false : true} speed={8}>
                <div className="">{offers[actOffer].desc}</div>
              </Parallax>
            </div>
            <div className="offer__img">
              <div className="offer__blocks_arrows">
                <button
                  onClick={() =>
                    actOffer > 0 ? setActOffer(actOffer - 1) : null
                  }
                  className="prev arrow"
                >
                  <img src={arrowComments} alt="" />
                </button>
                <button
                  onClick={() =>
                    actOffer < offers.length - 1
                      ? setActOffer(actOffer + 1)
                      : null
                  }
                  className="next arrow"
                >
                  <img src={arrowComments} alt="" />
                </button>
              </div>
              {/* {actOffer} */}
              <ParallaxBanner
                className="offer__img_pic"
                layers={[
                  {
                    image: "/offers/" + offers[actOffer].img,
                    speed: -10,
                  },
                ]}
                style={{ height: "100%" }}
              />
              {/* <img src={} alt="" /> */}
            </div>
          </div>
          <div className="offer__titles">
            {offers.map((el, i) => (
              <div
                className={actOffer === i ? "active" : ""}
                onClick={() => setActOffer(i)}
                key={el.id}
              >
                {el.id}
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Offer;
