import React from "react";
import portugal from "../../assets/imgs/portugal.webp";
import sriLanka from "../../assets/imgs/sriLanka.webp";
import jordan from "../../assets/imgs/jordan.webp";
import { Parallax, ParallaxBanner } from "react-scroll-parallax";
import america from "../../assets/imgs/america.webp";
import "../PeopleChoose/choose.css";
import arrowComments from "../../assets/imgs/arrow-comment.svg";
import { Navigation, A11y } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

const Choose = () => {
  const data = [
    {
      title: "Португалия",
      href: portugal,
      link: "https://verdant-shame-600.notion.site/781ac799698c4caabba150775e3b2dcf?pvs=4",
      desc: "Португалия - это страна бескрайних пляжей и солнечных дней, где история переплетается с современностью. Вы сможете гулять по узким улочкам Лиссабона, наслаждаться великолепными видами на океан с мыса Рока и попробовать великолепное португальское вино. Здесь каждый уголок оживленный и гостеприимный, словно приглашая вас на настоящее путешествие в прошлое и будущее.",
    },
    {
      title: "Шри-Ланка",
      href: sriLanka,
      link: "https://verdant-shame-600.notion.site/509be156dc36446a982b130c21215660?pvs=4",
      desc: "Шри-Ланка - это жемчужина Индийского океана, знаменитая своей невероятной природной красотой и культурным наследием. Здесь вы сможете понаблюдать за дикой природой в национальных парках, посетить древние буддийские храмы, а также отдохнуть на прекрасных тропических пляжах. Шри-Ланка - это страна, где моменты покоя и приключения идеально переплетаются.",
    },
    {
      title: "Иордания",
      href: jordan,
      link: "https://verdant-shame-600.notion.site/32ce21a7fcaf443cb5122909c47b9712?pvs=4",
      desc: "Иордания - это величественные долины и древние города, вдохновляющие нас своей богатой историей. Вас ожидают удивительные сокровища Петры, бескрайние песчаные дюны пустыни Вади Рум и дружелюбные улыбки местных жителей. Иордания - это путешествие в страну, где древние чудеса соседствуют с современностью.",
    },
    {
      title: "Америка",
      href: america,
      link: "https://verdant-shame-600.notion.site/790d3847aaf64969853a0c334818dd31",
      desc: "США - это мир впечатлений и разнообразия. От исключительной красоты национальных парков, таких как Гранд Каньон, до мегаполисов, таких как Нью-Йорк и Лос-Анджелес, Соединенные Штаты предлагают бесконечные возможности. Вы можете исследовать исторические памятники, попробовать разнообразные кухни и насладиться невероятными природными пейзажами.",
    },
  ];

  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);

  return (
    <section id="people-choose">
      <div className="container">
        <Parallax speed={5}>
          <h2 className="people-choose__title">
            <span>Люди</span> выбирают
          </h2>
        </Parallax>
      </div>
      <div className="people-choose__places">
        {data.map((el, i) => (
          <div className="people-choose__place" key={i}>
            <ParallaxBanner
              layers={[
                {
                  image: el.href,
                  speed: -10,
                },
              ]}
              style={{ height: "100%" }}
            />
            {/* <img src={el.href} alt="" /> */}
            <div className="people-choose__txt">
              <div className="people-choose__name">{el.title}</div>
              <div className="people-choose__desc">{el.desc}</div>
              <div className="people-choose__link">
                <a rel="noreferrer" target="_blank" href={el.link}>
                  Просмотреть маршрут
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="7"
                    viewBox="0 0 16 7"
                    fill="none"
                  >
                    <path
                      d="M0.11499 3.77778H15.2038M15.2038 3.77778L11.2097 1M15.2038 3.77778L11.2097 6"
                      stroke="white"
                      strokeWidth="0.6"
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className="people-choose__places-slider">
        <div className="people-choose__blocks_arrows">
          <button ref={navigationPrevRef} className="prev arrow">
            <img src={arrowComments} alt="" />
          </button>
          <button ref={navigationNextRef} className="next arrow">
            <img src={arrowComments} alt="" />
          </button>
        </div>

        <Swiper
          modules={[Navigation, A11y]}
          spaceBetween={50}
          slidesPerView={1}
          navigation={{
            prevEl: navigationPrevRef.current,
            nextEl: navigationNextRef.current,
          }}
          onBeforeInit={(swiper) => {
            swiper.params.navigation.prevEl = navigationPrevRef.current;
            swiper.params.navigation.nextEl = navigationNextRef.current;
          }}
        >
          {data.map((el, i) => (
            <SwiperSlide className="people-choose__place" key={i}>
              {/* <img src={el.href} alt="" /> */}
              <ParallaxBanner
                layers={[
                  {
                    image: el.href,
                    speed: 10,
                  },
                ]}
                style={{ height: "100%" }}
              />
              <div className="people-choose__txt">
                <div className="people-choose__name">{el.title}</div>
                <div className="people-choose__desc">{el.desc}</div>
                <div className="people-choose__link">
                  <a href={el.link}>
                    В путь
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="7"
                      viewBox="0 0 16 7"
                      fill="none"
                    >
                      <path
                        d="M0.11499 3.77778H15.2038M15.2038 3.77778L11.2097 1M15.2038 3.77778L11.2097 6"
                        stroke="white"
                        strokeWidth="0.6"
                      />
                    </svg>
                  </a>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </section>
  );
};

export default Choose;
