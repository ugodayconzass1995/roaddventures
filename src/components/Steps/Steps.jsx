import React from "react";
import stepsImg from "../../assets/imgs/steps.jpg";
import { Parallax, ParallaxBanner } from "react-scroll-parallax";
import "../Steps/steps.css";
import bg from "../../assets/imgs/steps.jpg";

const Steps = () => {
  const data = [
    {
      id: 1,
      title: "Связаться с нами",
      desc: "Необходимо заполнить специально разработанную нами анкету, где будут отражены все важные вопросы о предстоящем туре. В случае, если вам необходима помощь в выборе направления, необходимо это указать, либо вдохновиться нашими готовыми маршрутами.",
    },
    {
      id: 2,
      title: "Наше предложение",
      desc: "После изучения ваших предпочтений и ответов, мы разрабатываем предварительный маршрут. Вы получите доступ к нему через удобное приложение Notion, где сможете увидеть детали и внести свои комментарии.",
    },
    {
      id: 3,
      title: "Подписание договора & детальное планирование",
      desc: "Мы подписываем Договор об оказываемых услугах и убедившись в том, что мы внимательно учли все ваши пожелания, мы переходим к детальной планировке маршрутов. Это включает в себя организацию трансферов, выбор размещения, а также подбор разнообразных развлечений.",
    },
    {
      id: 4,
      title: "Готовимся к путешествию",
      desc: "Теперь, когда у нас есть всё необходимое для вашей поездки, вы можете спокойно готовиться к приключению, зная, что мы всегда здесь, чтобы поддержать вас. Не стесняйтесь обращаться в любой момент, если у вас возникнут вопросы. Пожелаем вам приятного путешествия и незабываемых впечатлений! :)",
    },
  ];

  return (
    <section id="steps">
      <div className="container steps__desktop">
        <div className="steps-col_1">
          <Parallax speed={10}>
            <img src={stepsImg} alt="" />
          </Parallax>
          <div className="steps__block">
            {[data[1], data[3]].map((el) => (
              <Parallax speed={10} key={el.id}>
                <div className="steps__step">
                  <div className="steps__number">{el.id}</div>
                  <div className="steps__name">{el.title} </div>
                  <div className="steps__desc">{el.desc} </div>
                </div>
              </Parallax>
            ))}
          </div>
        </div>
        <div className="steps-col_2">
          <Parallax speed={10}>
            <h2 className="steps__title">
              <span>HOW</span>
              it works
            </h2>
          </Parallax>
          <div className="steps__block">
            {[data[0], data[2]].map((el) => (
              <Parallax speed={15} key={el.id}>
                <div className="steps__step">
                  <div className="steps__number">{el.id}</div>
                  <div className="steps__name">{el.title} </div>
                  <div className="steps__desc">{el.desc} </div>
                </div>
              </Parallax>
            ))}
          </div>
        </div>
      </div>
      <div className="steps__mobile">
        <div className="steps__mobile_head">
          <ParallaxBanner
            layers={[
              {
                image: bg,
                speed: -10,
              },
            ]}
            className="steps__mobile_bg"
          />
          {/* <img src={bg} alt="" /> */}
          <Parallax speed={4}>
            <h2 className="steps__title">
              <span>HOW</span>
              it works
            </h2>
          </Parallax>
        </div>
        <div className="container">
          <Parallax speed={10}>
            <div className="steps__block">
              {data.map((el) => (
                <div className="steps__step">
                  <div className="steps__number">{el.id}</div>
                  <div className="steps__name">{el.title} </div>
                  <div className="steps__desc">{el.desc} </div>
                </div>
              ))}
            </div>
          </Parallax>
        </div>
      </div>
    </section>
  );
};

export default Steps;
