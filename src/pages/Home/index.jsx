import React from "react";
import { Parallax } from "react-scroll-parallax";
import Header from "../../components/Header/Header";
import Agenda from "../../components/Agenda/Agenda";
import Offer from "../../components/Offer/Offer";
import Steps from "../../components/Steps/Steps";
import Choose from "../../components/PeopleChoose/Choose";
import Book from "../../components/Book/Book";
import Reviews from "../../components/Reviews/Reviews";
import giraffe from "../../assets/imgs/giraffe.png";
import Footer from "../../components/Footer/Footer";
import Faq from "../../components/Faq/Faq";
import "./index.css";

const HomePage = () => {
  return (
    <div className="page-wrapper">
      <Header />
      <Parallax speed={50} id="giraffe">
        <img src={giraffe} alt="" />
      </Parallax>
      <main>
        <Agenda />
        <Offer />
        <Choose />
        <Steps />
        <Book />
        <Reviews />
        <Faq />
      </main>
      <Footer />
    </div>
  );
};

export default HomePage;
